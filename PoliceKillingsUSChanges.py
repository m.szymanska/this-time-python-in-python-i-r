# -*- coding: utf-8 -*-
"""
Created on Sat Jun 13 19:26:24 2020

@author: Michalina Szymańska, Sebastian Łakomiec"""

# ładowanie niezbędnych pakietów
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime
from sklearn.linear_model import LinearRegression
import seaborn as sns

death = pd.read_csv("dane/PoliceKillingsUS.csv", encoding='mac_roman',\
                    parse_dates=["date"])

# usunięcie wszystkich wierszy "NA", "<NA>"
death=death.dropna(axis=0, how='any')
# df.dropna(subset = ['column_name']) 


## pozbywanie się wierszy z roku 2020 (przeniesione z 206), rok nie pełny
dateStop = "2020-01-01"
dateTimeStop = datetime.datetime.strptime(dateStop, '%Y-%m-%d')
death = death[death.date < dateTimeStop]

death.info()
# zmiana typów danych z 'object' główne na string, (oraz int)
death.id = death.id.astype("int")
death.name = death.name.astype("string")
death.manner_of_death = death.manner_of_death.astype("string")
death.armed = death.armed.astype("string")

# ValueError: Cannot convert non-finite values (NA or inf) to integer
death.age = death.age.astype("int")
death.gender = death.gender.astype("category")
death.race = death.race.astype("category")
death.city = death.city.astype("string")
death.state = death.state.astype("string")
death.threat_level = death.threat_level.astype("string")
death.flee = death.flee.astype("string")
death.signs_of_mental_illness = death.signs_of_mental_illness.astype("int")
death.body_camera = death.body_camera.astype("int")

death.info()

# Czyszczenie zbioru danych
print("\n\n")
# "name"
ileImion = death["name"].value_counts()
ileImion.iloc[np.argsort(ileImion.index)]

# wsród imion występuje wartosć "TK TK", która oznacza, że prawidłowa wartosć
# ma zostać dodana później
# Zakładamy, że imię nie wpływa na rozkłady danych przestępczosci
# tak samo jak numer indeksu i powód samej śmierci 
# (95 % zastrzelony oraz reszta zastrzelony i paralizator)
del death["id"]
del death["name"]
del death["manner_of_death"]

# Miasta nie były analizowane 
del death["city"]

ileBroni = death["armed"].value_counts()
ileBroni.iloc[np.argsort(ileBroni.index)].sort_values(ascending=False)


# ujednolicenie dla wartosci broni, okrelsonej jako "nieznana", 
for index, val in death["armed"].iteritems():
    if (val == "undetermined" or val == "unknown weapon"):
        death.loc[index, "armed"] = "unknown"
    
print("\n")
for kolumna in death:
    print("\nNazwa kolumny:", kolumna)
    count = death[kolumna].value_counts()
    print(count.iloc[np.argsort(count.index)].sort_values(ascending=False))

# age, zakres 6(?) - 91
# gender, M ~4k, F ~200
# race, W~2k, B~1k, H ~0,7k, A=83, N=70, O=43
# state
# mental illness, false ~3.3k, true ~1k
# threat, attack 3k, other 1.3k, undtermined ~166
# flee
# camera


# zmiany słowne threat_level na liczby 
for index, val in death["threat_level"].iteritems():
    if (val == "attack"):
        death.loc[index, "threat_level"] = "2"
    if (val == "other"):
        death.loc[index, "threat_level"] = "1"
    if (val == "undetermined"):
       death.loc[index, "threat_level"] = "0"

## dodanie kolumny "aggresive" na podstawie 'threat_level' oraz 'armed'
aggresive = [] 

for index, val in death["threat_level"].iteritems():    
    if (death.loc[index, "armed"] != "unarmed" and val == "2"):
        aggresive.append(1)
    else:
        aggresive.append(0)
  
death['aggresive'] = aggresive
death 

ileAggres = death["aggresive"].value_counts()
ileAggres.iloc[np.argsort(ileAggres.index)].sort_values(ascending=False)
   
deathV2 = []  
deathV2 = pd.DataFrame(deathV2)
deathV2["threat_level"] = death["threat_level"].copy()
deathV2["age"] = death["age"].copy()
deathV2["aggresive"] = death["aggresive"].copy()
deathV2["gender"] = death["gender"].copy()
deathV2["race"] = death["race"].copy()
deathV2["signs_of_mental_illness"]=death["signs_of_mental_illness"].copy()
deathV2.info()

# ilosć zabójstw w miesiącach w latach
iloscZgonowLataMiesiace = []
month, year = [], [];
for rok in range(2015, 2021):
    for miesiac in range(1, 13):
        dateTimeStr = str(rok) + "-" + str(miesiac) + "-01"
        dateTime = datetime.datetime.strptime(dateTimeStr, '%Y-%m-%d')
        dateTimeStrEnd = str(rok) + "-" + str(miesiac+1) + "-01"
        try:
            dateTimeEnd = datetime.datetime.strptime(dateTimeStrEnd, '%Y-%m-%d')
        except:
            dateTimeStrEnd = str(rok+1) + "-" + str(miesiac-11) + "-01"
            dateTimeEnd = datetime.datetime.strptime(dateTimeStrEnd, '%Y-%m-%d')
        
        for index, val in death["date"].iteritems():
            if(val >= dateTime and val < dateTimeEnd):
                month.append(miesiac)
                year.append(rok)

deathV2["month"] = month
deathV2["year"] = year

# zmiana skrótów użytych okresleń rasowych na liczby
# W: White, non-Hispanic 0  # B:Black, non-Hispanic 1
# A:Asian 2 # H:Hispanic 3 # O:Other 4 # None:unknown 5
death.race = death.race.astype("string")
for index, val in death["race"].iteritems():
    if (val == "W"):
        death.loc[index, "race"] = "0"
    if (val == "B"):
        death.loc[index, "race"] = "1"
    if (val == "A"):
        death.loc[index, "race"] = "2"
    if (val == "H"):
        death.loc[index, "race"] = "3"
    if (val == "O"):
        death.loc[index, "race"] = "4"
    if (val == "N"):
        death.loc[index, "race"] = "5"

deathV2["race"] = death["race"].copy()
deathV2.race = deathV2.race.astype("int")

# wizualizacja skupień przy pomocy seaborn pairplot
# ale tylko dla wartosci liczbowych
sns.pairplot(deathV2)
plt.savefig("duzy.jpg", dpi=300)
plt.show()


# wykres ilości średniej wieku w latach
# sredni wiek na przestrzeni lat 2015 - 2020
srWiekLata = []
for rok in range(2015, 2021):
    dateTimeStr = str(rok) + "-01-01"
    dateTime = datetime.datetime.strptime(dateTimeStr, '%Y-%m-%d')
    dateTimeStrEnd = str(rok+1) + "-01-01"
    dateTimeEnd = datetime.datetime.strptime(dateTimeStrEnd, '%Y-%m-%d')
    rokLista = []
    
    for index, val in death["date"].iteritems():
        if(val >= dateTime and val < dateTimeEnd):
            rokLista.append(death["age"][index])
    srWiekLata.append([rok, np.mean(rokLista)])


xAxisSrWiekLata, yAxisSrWiekLata = [], []
plt.title('Średnia wieku w poszczególnych latach.')
for x, y in srWiekLata:
    xAxisSrWiekLata.append(x)
    yAxisSrWiekLata.append(y)

plt.plot(xAxisSrWiekLata, yAxisSrWiekLata)
plt.xlabel('Przedział')
plt.ylabel('Liczba obserwacji')
plt.savefig("sredniWiekWszyscy.jpg", dpi=300)
plt.show()


# sredni wiek w poszczególnych miesiącach i latach
iloscZgonowLataMiesiace = []
ileLatRokMiesiac = []

for rok in range(2015, 2020):
    for miesiac in range(1, 13):
        dateTimeStr = str(rok) + "-" + str(miesiac) + "-01"
        dateTime = datetime.datetime.strptime(dateTimeStr, '%Y-%m-%d')
        dateTimeStrEnd = str(rok) + "-" + str(miesiac+1) + "-01"
        try:
            dateTimeEnd = datetime.datetime.strptime(dateTimeStrEnd, '%Y-%m-%d')
        except:
            dateTimeStrEnd = str(rok+1) + "-" + str(miesiac-11) + "-01"
            dateTimeEnd = datetime.datetime.strptime(dateTimeStrEnd, '%Y-%m-%d')
        count = 0
        temp = 0
        
        for index, val in death["date"].iteritems():
            if(val >= dateTime and val < dateTimeEnd):
                count += 1
                temp += death["age"][index]
        count = temp/count
        iloscZgonowLataMiesiace.append([str(rok)+"-"+str(miesiac), count])


xAxisZgonRokMiesiac, yAxisZgonRokMiesiac = [], []
for x, y in iloscZgonowLataMiesiace:
    xAxisZgonRokMiesiac.append(x)
    yAxisZgonRokMiesiac.append(y)

barWidth = 0.15
 
# set height of bar
bars1 = [yAxisZgonRokMiesiac[i] for i in range(len(yAxisZgonRokMiesiac)) if i < 12 ]
bars2 = [yAxisZgonRokMiesiac[i] for i in range(len(yAxisZgonRokMiesiac)) if i >= 12 and i<24 ]
bars3 = [yAxisZgonRokMiesiac[i] for i in range(len(yAxisZgonRokMiesiac)) if i >= 24 and i<36 ]
bars4 = [yAxisZgonRokMiesiac[i] for i in range(len(yAxisZgonRokMiesiac)) if i >= 36 and i<48 ]
bars5 = [yAxisZgonRokMiesiac[i] for i in range(len(yAxisZgonRokMiesiac)) if i >= 48 and i<60 ]
 
# Set position of bar on X axis
r1 = np.arange(len(bars1))
r2 = [x + barWidth for x in r1]
r3 = [x + barWidth for x in r2]
r4 = [x + barWidth for x in r3]
r5 = [x + barWidth for x in r4]

# Make the plot
plt.bar(r1, bars1, color='#7f6d5f', width=barWidth, edgecolor='white', label='var1')
plt.bar(r2, bars2, color='#557f2d', width=barWidth, edgecolor='white', label='var2')
plt.bar(r3, bars3, color='#2d7f5e', width=barWidth, edgecolor='white', label='var3')
plt.bar(r4, bars4, color='#f00000', width=barWidth, edgecolor='white', label='var4')
plt.bar(r5, bars5, color='#ffc0cb', width=barWidth, edgecolor='white', label='var5')
 
# Add xticks on the middle of the group bars
plt.xlabel('group', fontweight='bold')
plt.xticks([r + barWidth for r in range(len(bars1))], ['sty', 'luty', \
        'mar', 'kwi', 'maj', 'cze', 'lip', 'sie', 'wrze', 'paz', 'lis', 'gru'])
 
axes = plt.gca()
axes.set_ylim([0, 42])
plt.title('sredni wiek w poszczególnych miesiącach i latach')
plt.xlabel("Miesiac / lata")
plt.ylabel("Średni wiek")
plt.savefig("sredniWiek.jpg", dpi=300)
plt.show()


# sami nie agresywni
# ilosć zabójst w misiącach w latach
iloscZgonowLataMiesiace = []

for rok in range(2015, 2020):
    for miesiac in range(1, 13):
        dateTimeStr = str(rok) + "-" + str(miesiac) + "-01"
        dateTime = datetime.datetime.strptime(dateTimeStr, '%Y-%m-%d')
        dateTimeStrEnd = str(rok) + "-" + str(miesiac+1) + "-01"
        try:
            dateTimeEnd = datetime.datetime.strptime(dateTimeStrEnd, '%Y-%m-%d')
        except:
            dateTimeStrEnd = str(rok+1) + "-" + str(miesiac-11) + "-01"
            dateTimeEnd = datetime.datetime.strptime(dateTimeStrEnd, '%Y-%m-%d')
        count = 0;
        
        for index, val in death["date"].iteritems():
            if (val >= dateTime and val < dateTimeEnd):
                if (death.loc[index, "aggresive"] == 0):
                    count += 1
        iloscZgonowLataMiesiace.append([str(rok)+"-"+str(miesiac), count])


xAxisZgonRokMiesiac, yAxisZgonRokMiesiac = [], []
for x, y in iloscZgonowLataMiesiace:
    xAxisZgonRokMiesiac.append(x)
    yAxisZgonRokMiesiac.append(y)


barWidth = 0.15
 
# set height of bar
bars1 = [yAxisZgonRokMiesiac[i] for i in range(len(yAxisZgonRokMiesiac)) if i < 12 ]
bars2 = [yAxisZgonRokMiesiac[i] for i in range(len(yAxisZgonRokMiesiac)) if i >= 12 and i<24 ]
bars3 = [yAxisZgonRokMiesiac[i] for i in range(len(yAxisZgonRokMiesiac)) if i >= 24 and i<36 ]
bars4 = [yAxisZgonRokMiesiac[i] for i in range(len(yAxisZgonRokMiesiac)) if i >= 36 and i<48 ]
bars5 = [yAxisZgonRokMiesiac[i] for i in range(len(yAxisZgonRokMiesiac)) if i >= 48 and i<60 ]
 
# Set position of bar on X axis
r1 = np.arange(len(bars1))
r2 = [x + barWidth for x in r1]
r3 = [x + barWidth for x in r2]
r4 = [x + barWidth for x in r3]
r5 = [x + barWidth for x in r4]

# Make the plot
plt.bar(r1, bars1, color='#7f6d5f', width=barWidth, edgecolor='white', label='var1')
plt.bar(r2, bars2, color='#557f2d', width=barWidth, edgecolor='white', label='var2')
plt.bar(r3, bars3, color='#2d7f5e', width=barWidth, edgecolor='white', label='var3')
plt.bar(r4, bars4, color='#f00000', width=barWidth, edgecolor='white', label='var4')
plt.bar(r5, bars5, color='#ffc0cb', width=barWidth, edgecolor='white', label='var5')
 
# Add xticks on the middle of the group bars
plt.xlabel('group', fontweight='bold')
plt.xticks([r + barWidth for r in range(len(bars1))], ['sty', 'luty', \
               'mar', 'kwi', 'maj', 'cze', 'lip', 'sie', 'wrze', 'paz', 'lis', 'gru'])

axes = plt.gca()
axes.set_ylim([0, 100])
plt.title('Liczba ofiar nieagresywnych w poszczególnych miesiąch i latach')
plt.xlabel("Miesiąc")
plt.ylabel("Liczba ofiar na miesiąc w latach")
plt.savefig("NonAgresive.jpg", dpi=300)
plt.show()


# analiza
# age od month   ii, age/mean, count
srWiekMiesiace = [[i, 0, 0] for i in range(1,13)]

for miesiac in range(1, 13):
    for index, val in deathV2["month"].iteritems():
        if (val == miesiac and deathV2["aggresive"][index] == 0):
            age = srWiekMiesiace[miesiac-1][1]
            count = srWiekMiesiace[miesiac-1][2]
            srWiekMiesiace[miesiac-1][1] = age + deathV2["age"][index]
            srWiekMiesiace[miesiac-1][2] = count + 1

xAx, yAx = [], []
for i in range(0, 12):
    srednia = srWiekMiesiace[i][1]/srWiekMiesiace[i][2]
    srWiekMiesiace[i][1] = srednia
    xAx.append(i+1)
    yAx.append(srednia)

plt.plot(xAx, yAx)
plt.title("srednia wieku ofiar nieagresywnych w poszczególnych miesiącach")
plt.xlabel("numer miesiaca")
plt.ylabel("srednia wieku")
plt.savefig("SredniaWiekuDlaMiesiecyNonAgresive.jpg", dpi=300)
plt.show()

model = LinearRegression()
xArray = np.array(xAx) 
yArray = np.array(yAx)
model.fit(xArray.reshape((-1, 1)), yArray)
print("slope:", model.coef_)


# "race" w danych miesiacach
rasaMiesiace = [[0, 0, 0, 0, 0, 0] for i in range(1,13)]
for miesiac in range(0, 12):
    for index, val in deathV2["race"].iteritems():
        if ((miesiac+1) == deathV2["month"][index]):
            count = rasaMiesiace[miesiac][val]
            rasaMiesiace[miesiac][val] = count + 1

xAx, yAx = [i for i in range(1, 13)], []
for y in rasaMiesiace:
    yAx.append(y)

plt.plot(xAx, yAx)
plt.title("Liczba ofiar wg ras w miesiącach")
plt.xlabel("Numer miesiąca")
plt.ylabel("Liczba ofiar")
plt.legend("WBAHOU",loc='upper right')
plt.savefig("LiczbaOfiarMiesiace.jpg", dpi=300)
plt.show()

# osobno rasy
def podzialNaRasy(raceNumber):
    print("\n\n\nDla rasy o numerze", raceNumber)
    specific = deathV2.copy()
    specific["state"] = death["state"].copy()
    specific["armed"] = death["armed"].copy()
    specific["flee"] = death["flee"].copy()
    specific["threat_level"] = death["threat_level"].copy()
    specific["signs_of_mental_illness"]=death["signs_of_mental_illness"].copy()
    
    ## tylko wybrana rasa
    specific = specific[specific.race == raceNumber]
    
    count = specific["age"].value_counts()
    count = count.iloc[np.argsort(count.index)].sort_values(ascending=False)
    srednia = [] 
    for index, count in count.iteritems():
        for i in range(count):
            srednia.append(index)
        
    print("Średni wiek to:", round(np.mean(srednia), 3), "lat")
    
    count = specific["state"].value_counts()
    #count = count.iloc[np.argsort(count.index)].sort_values(ascending=False)
    for index, value in count.iteritems():
        print("Najczęstszy stan to:",index,"\n")
        break
    
    
    count = specific["armed"].value_counts()
    counter = 0
    for index, value in count.iteritems():
        counter += 1
        print("Najczęstsze uzbrojenie to: ", index)
        if (counter >= 3):
            print()
            break
    
    
    count = specific["month"].value_counts()
    counter = 0
    for index, value in count.iteritems():
        counter += 1
        print("Najczęsciej w miesiącu:", index)
        if (counter >= 3):
            print()
            break
        
        
    count = specific["flee"].value_counts()
    print("Sposób ucieczki (procent szans)")
    suma = 0
    for i in range(len(count)):
        suma += count[i]
    counter = 0
    for index, value in count.iteritems():
        counter += 1
        print(counter, " :", index, "~", round(value/suma*100, 2), "%")
        
    
    count = specific["signs_of_mental_illness"].value_counts()
    print("\nZazwyczaj", end = ' ')
    for index, value in count.iteritems():
        if index == 0:
            print("zdrowi psychicznie", end = ' ')
        else:
            print("chorzy", end = ' ')
        print("(\"mental illness\")\n")
        break
    
    
    count = specific["aggresive"].value_counts()
    print("Zazwyczaj są", end = ' ')
    for index, value in count.iteritems():
        if index == 0:
            print("stosunkowo nie groźni", end = ' ')
        else:
            print("niebezpieczni\n")
        break

    
for i in range(6):
    podzialNaRasy(i)


# porównanie non agsresive / aggresive
countRaceAgr = [0, 0, 0, 0, 0, 0]
countRaceNonAgr = [0, 0, 0, 0, 0, 0]
for race in range(0, 6):
    for index, val in deathV2["race"].iteritems():
        if (val == race):#dobra rasa
            if (deathV2["aggresive"][index] == 0):
                countRaceNonAgr[race] += 1
            else:
                countRaceAgr[race] += 1
                
A, B = countRaceAgr, countRaceNonAgr
X = range(6)

plt.bar(X, A, color = 'r')
plt.bar(X, B, color = 'g', bottom = A)
plt.xlabel("Indeks rasy")
plt.ylabel("Ilość osób w danej rasie")
plt.legend(["Aggresive", "Non-Aggresive"])
plt.savefig("StosunekAgresywnychDoNieAgr.jpg", dpi=300)
plt.show()

